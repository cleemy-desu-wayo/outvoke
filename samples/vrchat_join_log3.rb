# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# ----
# This sample works much like samples/vrchat_join_log.rb.
# 
# In VRChat client version 2024.4.1 (Build 1533), the log file output format has changed.
# 
# https://docs.vrchat.com/docs/vrchat-202441
# The above page contains the following text:
# 
# > Output logs now include user IDs when players join or leave an instance.
# 
# So in this sample code, user IDs are output in an easily readable format.
# ----
#

using OutvokeDSL

hook 'vrchat-001', /\A\[behaviour\] entering room: (.*)/i do |e|
  e.status.count = 0
  "new world name      | #{e.m[1]}"
end

hook 'vrchat-001', /\A\[behaviour\] joining (wrld_[-0-9a-z]+):([-_0-9a-z]+).*~(region.*)/i do |e|
  "new world instance  | #{e.m[2]} - #{e.m[3]} - #{e.m[1]}"
end

hook 'vrchat-001', /\A\[behaviour\] joining (wrld_[-0-9a-z]+):([-_0-9a-z]+)$/i do |e|
  "new world instance  | #{e.m[2]} - region(?) - #{e.m[1]}"
end

hook 'vrchat-001', /\A\[behaviour\] (connected to .*)/i do |e|
  "connect             | #{e.m[1]}"
end

hook 'vrchat-001', /\A\[behaviour\] (switching to network .*)/i do |e|
  "switch network      | #{e.m[1]}"
end

hook 'vrchat-001', /\A\[behaviour\] onplayerjoined (.+) \((usr_[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\)/i do |e|
  e.status.count += 1
  count_str = sprintf("%5d", e.status.count)
  "joined!!!   | #{count_str} | #{e.m[2]} | #{e.m[1]}"
end

hook 'vrchat-001', /\A\[behaviour\] onplayerleft (.+) \((usr_[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\)/i do |e|
  e.status.count -= 1
  e.status.count = 0 if e.status.count < 0
  count_str = sprintf("%5d", e.status.count)
  "left...     | #{count_str} | #{e.m[2]} |                                  | #{e.m[1]}"
end

hook 'vrchat-001', /AcceptNotification .*Notification from username:(.*), sender user id:usr_.* of type: *([^,]+),/i do |e|
  "accept notification | username:#{e.m[1]} - type:#{e.m[2]}"
end
