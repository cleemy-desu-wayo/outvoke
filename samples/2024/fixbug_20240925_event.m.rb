# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-09-25
# --------
#
# this is a sample code to check for the presence of a bug that is fixed in
# Outvoke version 0.0.99.20240925.
#

using OutvokeDSL

hook "every-sec", /.*/ do |e|
  "/.*/ (regex) --------------- e.body: #{e.body} ---- e.m[0]: #{e.m[0].to_s}"
end

hook "every-sec", ->(x){ x.body.match?(/ ..:..:.[05]/) } do |e|
  "/ ..:..:.[05]/ (proc) ------ e.body: #{e.body} ---- e.m[0]: #{e.m[0].to_s}"
end
