# for Outvoke 0.1 (version 0.0.99.20241109 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-09
# --------
#
# a minimal sample of "to" method (without a receiver)
#
# usage:
#
#   $ seq 1 10 | ./outvoke.rb samples/2024/tomethod_minsample1.rb
#

using OutvokeDSL

# in Outvoke version 0.0.99.20241107.1 or earlier, the following method is active
def to(x)
  loputs "**** this is from top level method to() **** ---- #{x}"
end

hook "stdin", /\A[0-9]+\z/ do
  to "lo"
end

hooklo do |e|
  "hooklo: #{e.body}"
end
