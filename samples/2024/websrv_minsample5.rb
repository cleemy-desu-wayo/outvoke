# for Outvoke 0.1 (version 0.0.99.20241127 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-27
# --------
#
# a sample of data source "web-001"
#
# in this example, static page and dynamic page are mixed.
#

using OutvokeDSL

$outvoke.wait = 0.1

$outvoke["web-001"].then do
  _1.port           = 8081
  _1.document_root  = "./"
  _1.mount_proc_dir = "/dynamic"
  _1.enabled        = true
end

#
# try $ wget "http://localhost:8081/dynamic?num=55" -q -S -O -
#
hookweb do |e|
  e.get["num"].to_i * 2
end
