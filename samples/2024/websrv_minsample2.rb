# for Outvoke 0.1 (version 0.0.99.20241127 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-27
# --------
#
# a sample of data source "web-001"
#

using OutvokeDSL

$outvoke.wait = 0.1

#
# show request headers 1
# try $ wget http://localhost:8080/test1 -q -S -O -
#
hook "web-001", /^\/test1$/ do |e|
  JSON.pretty_generate(e.req.header)
end

#
# show request headers 2
# try $ wget http://localhost:8080/test2 -q -S -O -
#
hook "web-001", /^\/test2$/ do |e|
  e.req.header.to_json
end

#
# show request headers 3
# try $ wget http://localhost:8080/test3 -q -S -O -
#
hook "web-001", /^\/test3$/ do |e|
  e.req.header.to_yaml
end

#
# treating request parameters 1
# try $ wget "http://localhost:8080/test4?num=100&name=john" -q -S -O -
#
hook "web-001", /^\/test4$/ do |e|
  JSON.pretty_generate(e.get)
end

#
# treating request parameters 2
# try $ wget "http://localhost:8080/test5?num=200" -q -S -O -
#
hook "web-001", /^\/test5$/ do |e|
  if e.get["num"].to_i > 100
    "ok"
  else
    "under 100"
  end
end

#
# treating request parameters 3
# try $ wget "http://localhost:8080/test6?num=200" -q -S -O -
#
# if i is 100 or less, it will return 500 Internal Server Error
#
hook "web-001", ->(e) { e.body == "/test6" && e.get["num"].to_i > 100 } do
  "ok!"
end
