# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-24
# --------
#
# this is a sample code to check for the presence of a bug that is fixed in
# Outvoke version 0.0.99.20241224.
#

using OutvokeDSL

hookvr /pickup/i
