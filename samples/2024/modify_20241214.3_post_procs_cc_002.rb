# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-14
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241214.3
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241214.3_post_procs_cc_002.rb
#

using OutvokeDSL

hook "every-sec", /..:..:.0/ do |e|
  sleep 2
  "#{e} -- #{Time.now}"
end

hookcc "every-sec", /..:..:.5/ do |e|
  sleep 2
  "#{e} -- #{Time.now}"
end
