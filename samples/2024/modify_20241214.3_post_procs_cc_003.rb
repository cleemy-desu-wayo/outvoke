# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-14
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241214.3
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241214.3_post_procs_cc_003.rb
#

using OutvokeDSL

$outvoke["every-sec"].post_procs_cc << ->(e, x) {
  return nil unless x
  puts "prot_procs_cc 1 : e = #{e} ---- x.time = #{x&.time}"
}

$outvoke["every-sec"].post_procs_cc << ->(e, x) {
  puts "prot_procs_cc 2 : e = #{e} ---- x.time = #{x&.time}"
  puts "----"
}

hookcc "every-sec", /..:..:.0/ do
  loputs e
  sleep 2
  e
end

hookcc "every-sec", /..:..:.4/ do
  true
end

hookcc "every-sec", /..:..:.8/ do
  loputs e
end

hooklo do
  puts "hooklo : #{e}"
  puts "----"
end
