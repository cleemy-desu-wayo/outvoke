# for Outvoke 0.1 (version 0.0.99.20241113 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-14
# --------
#
# show world information and user list
#
# this sample is based on samples/vrchat_join_log3.rb
#
# --------
#
# this version is current as of 2024-12-20 16:00 UTC
#
# history is here:
#   https://gitlab.com/cleemy-desu-wayo/outvoke/-/commits/main/samples/2024/webrick_vrchat_instanceinfo.rb
#
# --------
#
# this sample samples/2024/webrick_vrchat_instanceinfo.rb is the
# side that provides JSON data.
#
# about the side that reads JSON data, see below:
# https://gitlab.com/cleemy-desu-wayo/outvoke/-/blob/main/samples/2024/webrick_vrchat_instanceinfo.html
#
# --------
#
# * Usage:
#
#   1. start VRChat
#
#   2. launch Outvoke as follows:
#
#     $ ./outvoke.rb samples/2024/webrick_vrchat_instanceinfo.rb
#
#   3. try web access (if it works well, there should be JSON output)
#
#     $ wget http://localhost:8080/outvoke-vrchat-instanceinfo -q -S -O -
#
#
# * Description-en:
#
#   This sample displays information about the world you are in
#   and a list of the users who are currently joined.
#
#   These information is delivered in JSON via the web, rather than
#   to standard output.
#
#
# * Description-ja:
#
#   このサンプルは、今いるワールドに関する情報と、現在join中のユーザー
#   のリストを表示します。
#
#   これらの情報は標準出力ではなく、Web経由でJSONで配信されます。
#

using OutvokeDSL

require 'webrick'
require 'json'

def reset_instance_data(x)
  x          = Hash.new
  x["world"] = Hash.new
  x["users"] = Hash.new
  x
end

instance_data = nil
instance_data = reset_instance_data(instance_data)


# -------------- WEBrick part --------------

#
# try changing these variables to suit your environment
#
$webrick_instanceinfo_port  ||= 8080
#$webrick_instanceinfo_url   ||= "outvoke-" + (1..12).map{ "2348acefhjkmnpstwxyz".split("").sample }.join  # random
$webrick_instanceinfo_url   ||= "outvoke-vrchat-instanceinfo"
$webrick_instanceinfo_html  ||= "webrick_vrchat_instanceinfo.html"

$webrick_instanceinfo_cors_list  ||= []
#$webrick_instanceinfo_cors_list  << "*"  # make it readable from any website
$webrick_instanceinfo_cors_list  << "http://localhost:#{$webrick_instanceinfo_port}"
#$webrick_instanceinfo_cors_list  << "https://cleemy-desu-wayo.gitlab.io"

# referrer allow list (if you want to deny access based on referrer info)
$webrick_instanceinfo_ref   ||= []
#$webrick_instanceinfo_ref   << "http://localhost:#{$webrick_instanceinfo_port}/webrick_vrchat_instanceinfo.html"


url = "http://localhost:#{$webrick_instanceinfo_port}/#{$webrick_instanceinfo_url}"

puts "# "
puts "# if you are accessing from local, try #{url}"
puts "# "

if $webrick_instanceinfo_cors_list[0]
  puts "# --------"
  $webrick_instanceinfo_cors_list.each do |origin|
    puts "# allowed origin: #{origin}"
  end
  puts "# --------"
  puts "# "
end

if $webrick_instanceinfo_ref[0]
  puts "# --------"
  $webrick_instanceinfo_ref.each do |url|
    puts "# allowed referer: #{url}"
  end
  puts "# --------"
  puts "# "
end

websrv = WEBrick::HTTPServer.new({:Port => $webrick_instanceinfo_port})

websrv.mount_proc "/#{$webrick_instanceinfo_url}" do |req, res|
  cors_value = nil
  if $webrick_instanceinfo_cors_list.include?("*")
    if req["Origin"]
      cors_value = "*"
    end
  else
    cors_value = $webrick_instanceinfo_cors_list.find { _1 == req["origin"] }
  end

  res["Content-Type"]                = "text/html; charset=UTF-8"

  if cors_value && !cors_value.include?("\n")
    res["Access-Control-Allow-Origin"] = cors_value
  end

  res["Vary"]                        = "Origin"
  res["Cache-Control"]               = "no-store"
  res["Pragma"]                      = "no-cache"

  res_data = Hash.new.to_s

  if $webrick_instanceinfo_ref[0]
    if $webrick_instanceinfo_ref.include?(req["referer"])
      res_data = instance_data
    end
  else
    res_data = instance_data
  end

  res.body = JSON.pretty_generate(res_data)
end

websrv.mount_proc "/#{$webrick_instanceinfo_html}" do |req, res|
  res["Content-Type"] = "text/html; charset=UTF-8"
  res.body = File.read($webrick_instanceinfo_html)
end

Thread.new { websrv.start }


# -------------- Outvoke part --------------

hookft "vrchat-001", /\A\[behaviour\] entering room: (.*)/i do |e|
  instance_data["world"]["name"] = e.m[1]
  instance_data["users"] = Hash.new
  nil
end

hookft "vrchat-001", /\A\[behaviour\] joining (wrld_[-0-9a-z]+):([-_0-9a-z]+).*~region\((.*)\)/i do |e|
  instance_data["world"]["id"]       = e.m[1]
  instance_data["world"]["instance"] = e.m[2]
  instance_data["world"]["region"]   = e.m[3]
  nil
end

hookft "vrchat-001", /\A\[behaviour\] joining (wrld_[-0-9a-z]+):([-_0-9a-z]+)$/i do |e|
  instance_data["world"]["id"]       = e.m[1]
  instance_data["world"]["instance"] = e.m[2]
  instance_data["world"]["region"]   = "?"
  nil
end

hookft "vrchat-001", /\A\[behaviour\] onplayerjoined (.+) \((usr_[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\)/i do |e|
  instance_data["users"][e.m[2]] = e.m[1]
  nil
end

hookft "vrchat-001", /\A\[behaviour\] onplayerleft (.+) \((usr_[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\)/i do |e|
  instance_data["users"].delete(e.m[2])
  nil
end

hook "vrchat-001", /\A\[behaviour\] entering room: (.*)/i do |e|
  e.status.count = 0
  instance_data["world"]["name"] = e.m[1]
  instance_data["users"] = Hash.new
  "[VRChat] Entering Room: #{e.m[1]}"
end

hook "vrchat-001", /\A\[behaviour\] joining (wrld_[-0-9a-z]+):([-_0-9a-z]+).*~region\((.*)\)/i do |e|
  instance_data["world"]["id"]       = e.m[1]
  instance_data["world"]["instance"] = e.m[2]
  instance_data["world"]["region"]   = e.m[3]
  nil
end

hook "vrchat-001", /\A\[behaviour\] joining (wrld_[-0-9a-z]+):([-_0-9a-z]+)$/i do |e|
  instance_data["world"]["id"]       = e.m[1]
  instance_data["world"]["instance"] = e.m[2]
  instance_data["world"]["region"]   = "?"
  nil
end

hook "vrchat-001", /\A\[behaviour\] onplayerjoined (.+) \((usr_[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\)/i do |e|
  e.status.count += 1
  instance_data["users"][e.m[2]] = e.m[1]
  "[VRChat] joined: #{e.m[1]}"
end

hook "vrchat-001", /\A\[behaviour\] onplayerleft (.+) \((usr_[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\)/i do |e|
  e.status.count -= 1
  e.status.count = 0 if e.status.count < 0
  instance_data["users"].delete(e.m[2])
  "[VRChat] left: #{e.m[1]}"
end

hook "vrchat-001", "[Player] OnApplicationQuit" do
  instance_data = reset_instance_data(instance_data)
  "[VRChat] OnApplicationQuit"
end
