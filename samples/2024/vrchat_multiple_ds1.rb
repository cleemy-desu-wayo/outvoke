# for Outvoke 0.1 (version 0.0.99.20240521 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-06-01
# --------
#
# * Setup:
#
#   1. Save as "maoudamashii-inst-drum2-hat.wav" from  https://maou.audio/se_inst_drum2_hat/
#
#   2. Place this .wav file, outvoke.rb, and this sample (vrchat_multiple_ds1.rb) in the
#      current directory, and grant execute permission to outvoke.rb.
#      Then execute as follows.
#
#      $ ./outvoke.rb vrchat_multiple_ds1.rb
#
#   3. Start VRChat and go to "猫和室 - modern".
#      (World ID: wrld_3a201318-33c2-4c4c-8310-163552f80d63)
#
# * Description-en:
#
#   When this sample is started, the sound continues to play at an interval of approximately
#   once per second.
#   When you pick up a dango, the sound stops.
#   When you pick up a yunomi (japanese teacup), the sound starts again.
#
#   This sample handles multiple data sources, “every-sec” and “vrchat-001,” to sound once per
#   second.
#
#   VRChat world "猫和室 - modern" was chosen only as an example of a world with plenty of
#   objects to pick up. This sample may not work due to changes in the specifications of
#   VRChat or updates to the world. Even if this sample doesn't work as expected, please
#   do not contact the world creator.
#   (BTW, "猫" (neko) means cats, and "和室" (washitsu) means japanese room)
#
# * Description-ja:
#
#   このサンプルを起動すると、だいたい1秒に1回の間隔で音が鳴り続けます。
#   だんごをpickupすると音は止まります。
#   湯呑みをpickupすると再び音が鳴り始めます。
#
#   このサンプルでは、1秒に1回音を鳴らすために「every-sec」と「vrchat-001」という複数の
#   データソースを取り扱っています。
#
#   VRChatのワールド「猫和室 - modern」はあくまでもpickupするものが豊富にあるワールドの
#   例として選んだだけです。VRChatの仕様変更やワールドのアップデートにより、このサンプルは
#   動かなくなる可能性があります。もしこのサンプルが期待通りに動かなかったとしても、
#   ワールド制作者さんに問い合わせをしないようお願いします。
#

using OutvokeDSL

$outvoke.wait = 0        # if the load is heavy, you may want to change this to 0.01, etc
INST_VOLUME   = "1.0"    # set the volume of the sound
WAV_FILE_NAME = "maoudamashii-inst-drum2-hat.wav"

is_music_on = true

raise "FILE NOT FOUND: #{WAV_FILE_NAME}" unless File.readable?(WAV_FILE_NAME)

hook 'every-sec', /./ do
  if is_music_on
    spawn "play", "-v", INST_VOLUME, "-q", WAV_FILE_NAME, :err=>"/dev/null"
  end
  is_music_on
end

hook 'vrchat-001', /pickup object: .bds_yunomi.*/i do
  is_music_on = true
  "**MUSIC CONTROL** music start"
end

hook 'vrchat-001', /pickup object: .uguisu_3dango.*/i do
  is_music_on = false
  "**MUSIC CONTROL** music stop"
end
