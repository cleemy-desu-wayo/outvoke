# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-19
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241119
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241119_tomethod2.rb
#

using OutvokeDSL

str = "hello"

hookcc "every-sec" do
  next if $outvoke.elapsed > 3
  sleep 2
  str.upcase!    # object_id of str is not changed
end

hooksec do
  next if $outvoke.elapsed > 3
  puts str
  str.to "lo"
  puts "sleep start"
  sleep 4
  puts "sleep end"
end

# due to "sleep 4", the first execution is 4 seconds later
hooklo
