# for Outvoke 0.1 (version 0.0.99.20241121 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-23
# --------
#
# a sample of loputs, hookcc and msgid
#
# This sample works similar to samples/2024/vrchat_loputs_hookcc.rb.
# msgid was introduced in Outvoke version 0.0.99.20241121.
#
# usage:
#
#   1. launch Outvoke as follows:
#
#     $ ./outvoke.rb samples/2024/vrchat_loputs_hookcc_msgid.rb
#
#   2. pick up some objects in the world of VRChat
#

using OutvokeDSL

$outvoke.wait = 0.1

hookcc "vrchat-001", /pickup object...([^']*)/i do |e|
  object_name = e.m[1]
  loputs "(msgid: #{e.msgid}) - #{object_name} : hookcc start"

  sleep 2
  loputs "(msgid: #{e.msgid}) - #{object_name} : 2 seconds elapsed after pickup"
  sleep 3
  loputs "(msgid: #{e.msgid}) - #{object_name} : 5 seconds elapsed after pickup"
  sleep 5
  loputs "(msgid: #{e.msgid}) - #{object_name} : 10 seconds elapsed after pickup"

  loputs "(msgid: #{e.msgid}) - #{object_name} : hookcc end"
end

hooklo do |e|
  "loopback event - #{e.body}"
end
