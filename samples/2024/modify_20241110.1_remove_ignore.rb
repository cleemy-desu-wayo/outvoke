# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-10
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241110.1
#
# usage:
#
#   $ seq 1 10 | ./outvoke.rb samples/2024/modify_20241110.1_remove_ignore.rb
#

using OutvokeDSL

hook "stdin", /\A1\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  true
end

hook "stdin", /\A2\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  "true"
end

hook "stdin", /\A3\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  false
end

hook "stdin", /\A4\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  nil
end

hook "stdin", /\A5\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  1
end

hook "stdin", /\A6\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  {"log" => "hello"}
end

hook "stdin", /\A7\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  {"log_" => "hello"}
end

hook "stdin", /\A8\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  e.body * 2
end

hook "stdin", /\A9\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  e.body.to_i * 2
end

hook "stdin", /\A10\z/ do |e|
  puts "puts e.body ---- #{e.body}"
  [e.body.to_i * 2]
end
