# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-19
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241119
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241119_tomethod1.rb
#

using OutvokeDSL

hooksec do
  to "lo"
  @event_body.gsub!("0", "o")    # object_id of @event_body is not changed
  nil
end

hooklo
