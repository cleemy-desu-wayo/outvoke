# for Outvoke 0.1 (version 0.0.99.20240819 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-08-25
# --------
#
# a sample of loputs and hookcc
#
# This sample works similar to samples/2024/vrchat_loputs.rb.
#
# * Description-en:
#
#   After you pickup something in the VRChat world, output a string 2 seconds, 5 seconds,
#   and 10 seconds later. If you pick up something else before 10 seconds have passed,
#   you can easily see the benefit of multi-threading.
#
#   hookcc allows entire blocks to be executed in another thread. hookcc was introduced in
#   Outvoke version 0.0.99.20240817. "cc" stands for concurrent computing.
#
#   If you change the the part of this sample code "hookcc" to "hook", you will have a
#   version that does not use multi-threading.
#
#   The string passed to loputs can be captured with the hook "lo". "lo" stands for loopback.
#
#   In Outvoke version 0.0.99.20240818 or later,
#   hook "lo", /./ do ... end
#   can now be abbreviated as follows:
#   hooklo do ... end
#
#   Outvoke version 0.0.99.20240817 and version 0.0.99.20240818 have a bug, so it is
#   recommended to try version 0.0.99.20240819 or later.
#
# * Description-ja:
#
#   VRChatのワールド内で何かをpickupしてから、2秒後と5秒後と10秒後に文字列を出力します。
#   10秒経つ前に別の何かをpickupすれば、マルチスレッドの恩恵が分かりやすいかと思います。
#
#   hookcc により、ブロック全体を別スレッドで実行することが可能になります。Outvokeの
#   version 0.0.99.20240817 から hookcc が登場しました。「cc」は concurrent computing です。
#
#   このサンプルコードの「hookcc」の箇所を「hook」に変更すれば、マルチスレッドを使用しない
#   バージョンになります。
#
#   loputs に渡した文字列は、hook "lo" でキャプチャできます。
#   「lo」はループバックです。
#
#   Outvoke version 0.0.99.20240818 からは、
#   hook "lo", /./ do ... end
#   は以下のように省略した書き方が可能になりました。
#   hooklo do ... end
#
#   Outvokeの version 0.0.99.20240817 と version 0.0.99.20240818 にはバグがあるので、
#   version 0.0.99.20240819 あるいはそれ以降で試すことを推奨します。
#

using OutvokeDSL

$outvoke.wait = 0.1

hookcc "vrchat-001", /pickup object...([^']*)/i do |e|
  object_name = e.m[1]
  loputs "#{object_name} : hookcc start"

  sleep 2
  loputs "#{object_name} : 2 seconds elapsed after pickup"
  sleep 3
  loputs "#{object_name} : 5 seconds elapsed after pickup"
  sleep 5
  loputs "#{object_name} : 10 seconds elapsed after pickup"

  loputs "#{object_name} : hookcc end"
end

hooklo do |e|
  "loopback event - #{e.body}"
end
