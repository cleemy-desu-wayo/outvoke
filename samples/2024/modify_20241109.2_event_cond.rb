# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-09
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241109.1
# and version 0.0.99.20241109.2.
#
# usage:
#
#   $ { echo "aaa" ; echo "" ; echo "bbb" ; } | ./outvoke.rb samples/2024/modify_20241109.2_event_cond.rb
#

using OutvokeDSL

hookstd do |e|
  "hook1 #{e.body}"
end

hookstd /./ do |e|
  "hook2 #{e.body}"
end

hookstd // do |e|
  "hook3 #{e.body}"
end

hookstd true do |e|
  "hook4 #{e.body}"
end
