# for Outvoke 0.1 (version 0.0.99.20240528.1 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-05-30
# --------
#
# * Setup:
#
#   1. Place outvoke.rb, and this sample (vrchat_get_log_file_name.rb) in the
#      current directory, and grant execute permission to outvoke.rb.
#      Then execute as follows.
#
#      $ ./outvoke.rb vrchat_get_log_file_name.rb
#
#   2. Start VRChat and go to a world that has objects to pick up in it.
#
# * Description-en:
#
#   This sample outputs information only when you pick up something in VRChat.
#   You can use e.status to get the filename of the log file and the line number in the file.
#
# * Description-ja:
#
#   VRChat内で何かをpickupした時のみ情報を出力します。
#   e.status を使用することにより、ログファイルのファイル名や、そのファイルでの何行目なのかを
#   取得できます。
#

using OutvokeDSL

hook "vrchat-001", /pickup object: .([^']*)/i do |e|
  "pickup: #{e.m[1]} ---- file: #{File.basename(e.status.logfile)} ---- line: #{e.status.lineno}"
end
