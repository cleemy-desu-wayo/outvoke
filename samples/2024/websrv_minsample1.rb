# for Outvoke 0.1 (version 0.0.99.20241127 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-27
# --------
#
# a sample of data source "web-001"
#

using OutvokeDSL

$outvoke.wait = 0.1

#
# a tiny example
# try $ wget http://localhost:8080/test1 -q -S -O -
#
hook "web-001", /^\/test1$/ do
  "hello this is test1"
end

#
# e.res is an WEBrick::HTTPResponse object
# try $ wget http://localhost:8080/test2 -q -S -O -
#
hook "web-001", /^\/test2$/ do |e|
  e.res["Content-Type"] = "text/html; charset=UTF-8"
  e.res.body = "hello this is test2\n"
  e.res
end

#
# 307 Temporary Redirect
# try $ wget http://localhost:8080/test3 -q -S -O -
#
hook "web-001", /^\/test3$/ do |e|
  begin
    e.res.status = 307
    e.res.set_redirect WEBrick::HTTPStatus::TemporaryRedirect, "/test2"
  rescue => err
    puts "err: #{err}"
  end
  e.res
end
