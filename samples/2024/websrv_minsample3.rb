# for Outvoke 0.1 (version 0.0.99.20241127 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-27
# --------
#
# a sample of data source "web-001"
#

using OutvokeDSL

$outvoke.wait = 0.1

#
# try $ wget http://localhost:8080/test1 -q -S -O -
#
hookcc "web-001", /^\/test1$/ do
  loputs "hookcc start -- (sleep 4)"
  sleep 4
  res "sleep 4 -- #{Time.now}"    # this value probably will be ignored
  loputs "hookcc end -- (sleep 4)"
end
hookcc "web-001", /^\/test1$/ do
  loputs "hookcc start -- (sleep 2)"
  sleep 2
  res "sleep 2 -- #{Time.now}"
  loputs "hookcc end -- (sleep 2)"
end

#
# try $ wget http://localhost:8080/test2 -q -S -O -
#
hookcc "web-001", /^\/test2$/ do |e|
  loputs "hookcc start -- (/test2)"
  sleep 2
  e.res["Content-Type"] = "text/html; charset=UTF-8"
  e.res.body = "hello hookcc test2\n"
  res e.res
  loputs "hookcc end -- (/test2)"
end

hooklo
