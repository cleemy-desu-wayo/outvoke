# for Outvoke 0.1 (version 0.0.99.20241109 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-09
# --------
#
# a minimal sample of BasicObject#to
#
# usage:
#
#   $ ./outvoke.rb samples/2024/tomethod_minsample2.rb
#
#   (try just starting it without any standard input)
#

using OutvokeDSL

"1".to("lo")

hooklo do |e|
  e.body.to_i.succ.to "lo" if e.body.to_i < 10
  e.body
end
