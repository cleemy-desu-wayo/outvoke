# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-23
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241223.3
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241223.3_enabled.rb
#

using OutvokeDSL

$outvoke["lo"].enabled = 1
puts $outvoke["lo"].enabled?

exit
