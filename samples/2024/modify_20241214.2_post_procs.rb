# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-14
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241214.2
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241214.2_post_procs.rb
#

using OutvokeDSL

$outvoke["every-sec"].post_procs << ->(e, x) {
  if x.respond_to?(:time)
    x_time_str = "x.time : #{x.time}"
  else
    x_time_str = "x has no time"
  end

  puts "#{e} ---- #{x_time_str} ---- same object? #{e.object_id == x.object_id}"
}

hook "every-sec", /..:..:.[02468]/

hook "every-sec", /..:..:.[13579]/ do
  true
end
