# for Outvoke 0.1 (version 0.0.99.20241204.1 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-06-02
# --------
#
# this version is current as of 2024-12-21 21:00 UTC
#
# history of this sample is here:
#   https://gitlab.com/cleemy-desu-wayo/outvoke/-/commits/main/samples/2024/vrchat_multiple_ds2.rb
#
# --------
#
# This sample is a advanced version of samples/2024/vrchat_multiple_ds1.rb.
#
# * Setup:
#
#   1. Save .wav files.
#      * "maoudamashii-inst-drum1-kick.wav"  from https://maou.audio/se_inst_drum1_kick/
#      * "maoudamashii-inst-drum1-snare.wav" from https://maou.audio/se_inst_drum1_snare/
#      * "maoudamashii-inst-guitar08.wav"    from https://maou.audio/se_inst_guitar08/
#      * "maoudamashii-inst-guitar10.wav"    from https://maou.audio/se_inst_guitar10/
#      * "maoudamashii-inst-guitar13.wav"    from https://maou.audio/se_inst_guitar13/
#
#   2. Place these .wav files, outvoke.rb, and this sample (vrchat_multiple_ds2.rb) in the
#      current directory, and grant execute permission to outvoke.rb.
#      Then execute as follows.
#
#      $ ./outvoke.rb vrchat_multiple_ds2.rb
#
#   3. Start VRChat and go to "猫和室 - modern".
#      (World ID: wrld_3a201318-33c2-4c4c-8310-163552f80d63)
#
# * Description-en:
#
#   When you start this sample, the drums will start to play.
#   The sound will change as you pick up something sweet.
#
#     * dango ....................... Uguisu_3Dango.000 turns the kick on/off
#                                     Uguisu_3Dango.001 turns the snare on/off
#     * castella .................... Uguisu_Castella.000 turns the tempo down
#                                     Uguisu_Castella.001 turns the tempo up
#     * donut ....................... plays a guitar sound
#
#     * yunomi (japanese teacup) .... BDS_Yunomi.000 turns the volume down
#                                     BDS_Yunomi.001 turns the volume up
#
#   I think what would be practical in the direction of this sample would be DJ playing
#   or playing using a looper (live looping sampler) rather than ordinary playing of the
#   instrument.
#
#   Also, from a programming perspective, this is not so much a music-related sample as
#   it is a sample that handles multiple data sources, and also of preparing an original
#   data source that are not defined in the Outvoke itself.
#
#   VRChat world "猫和室 - modern" was chosen only as an example of a world with plenty of
#   objects to pick up. This sample may not work due to changes in the specifications of
#   VRChat or updates to the world. Even if this sample doesn't work as expected, please
#   do not contact the world creator.
#   (BTW, "猫" (neko) means cats, and "和室" (washitsu) means japanese room)
#
# * Description-ja:
#
#   このサンプルを起動すると、ドラムの音が鳴り始めます。
#   甘い物をpickupすると、音の鳴り方が変化します。
#
#     * だんご ................ Uguisu_3Dango.000 がキックのON/OFF
#                               Uguisu_3Dango.001 がスネアのON/OFF
#     * カステラ .............. Uguisu_Castella.000 がテンポダウン
#                               Uguisu_Castella.001 がテンポアップ
#     * ドーナッツ ............ ギターの音を鳴らす
#
#     * 湯呑み ................ BDS_Yunomi.000 が音量ダウン
#                               BDS_Yunomi.001 が音量アップ
#
#   このサンプルのような方向で実用的になるのは楽器の演奏というよりはDJプレイやルーパー
#   （ライブルーピングサンプラー）を使ったプレイということになるかと思います。
#
#   また、プログラミングの観点では、これは音楽関連のサンプルというよりは、複数のデータソースを
#   取り扱うサンプルであると同時に、Outvoke本体では定義されていない独自のデータソースを自前で
#   用意するというサンプルでもあります。
#
#   VRChatのワールド「猫和室 - modern」はあくまでもpickupするものが豊富にあるワールドの
#   例として選んだだけです。VRChatの仕様変更やワールドのアップデートにより、このサンプルは
#   動かなくなる可能性があります。もしこのサンプルが期待通りに動かなかったとしても、
#   ワールド制作者さんに問い合わせをしないようお願いします。
#

using OutvokeDSL

class OutvokeDataSourceEveryMoment < OutvokeDataSource

  def initialize(outvoke, label)
    super
    @status = Struct.new(:now, :last_time, :quiet_mode).new
    @status.last_time = Time.now.floor
  end

  def each_event
    @log_lines.each do |line|
      event = OutvokeEvent.new(line[1])
      event.time = line[0]
      event.status = @status
      event.status.now = Time.now

      yield event

      @status.last_time = line[0]
    end
    self
  end

  def before_each_event
    @log_lines = [[Time.now, Process.clock_gettime(Process::CLOCK_MONOTONIC)]]
  end

  def after_each_event
    nil
  end

  def first_time_log_check(ds, event)
    nil
  end
end

'every-moment'.then do
  $outvoke.sources[_1] = OutvokeDataSourceEveryMoment.new($outvoke, _1)
end

$outvoke.wait = 0        # if the load is heavy, you may want to change this to 0.01, etc
inst_volume   = 1.0      # set the volume of the sound

is_kick_on          = true
is_snare_on         = true
last_beat_base_time = nil
bpm                 = 80
beat_mode           = 1

wav = {
      'drum1_kick':  "maoudamashii-inst-drum1-kick.wav",
      'drum1_snare': "maoudamashii-inst-drum1-snare.wav",
      'guitar08':    "maoudamashii-inst-guitar08.wav",
      'guitar10':    "maoudamashii-inst-guitar10.wav",
      'guitar13':    "maoudamashii-inst-guitar13.wav"
}

wav.each do |label, file_name|
  raise "FILE NOT FOUND: #{file_name}" unless File.readable?(file_name)
end

hook 'every-moment' do |e|
  beat_interval = 60.0 / bpm * 4
  now = e.body.to_f
  inst_type = nil

  last_beat_base_time = now unless last_beat_base_time 

  if e.body.to_f >= last_beat_base_time + beat_interval
    inst_type = :drum1_kick if is_kick_on
    last_beat_base_time = now
    beat_mode = 2
  end
  if beat_mode == 2 && now >= last_beat_base_time + beat_interval * 0.25
    inst_type = :drum1_snare if is_snare_on
    beat_mode = 3
  end
  if beat_mode == 3 && now >= last_beat_base_time + beat_interval * 0.5
    inst_type = :drum1_kick if is_kick_on
    beat_mode = 4
  end
  if beat_mode == 4 && now >= last_beat_base_time + beat_interval * 0.625
    inst_type = :drum1_kick if is_kick_on
    beat_mode = 5
  end
  if beat_mode == 5 && now >= last_beat_base_time + beat_interval * 0.75
    inst_type = :drum1_snare if is_snare_on
    beat_mode = 1
  end

  spawn "play", "-v", "#{inst_volume}", "-q", wav[inst_type], :err=>"/dev/null" if inst_type
  nil
end

# output a object type
hook 'vrchat-001', /pickup object: .([^']*)/i do |e|
  e.m[1]
end

# kick on/off
hook 'vrchat-001', /pickup object: .Uguisu_3Dango\.000/i do
  is_kick_on = !is_kick_on
  "**MUSIC CONTROL** kick: #{is_kick_on ? "ON" : "OFF"}"
end

# snare on/off
hook 'vrchat-001', /pickup object: .Uguisu_3Dango\.001/i do
  is_snare_on = !is_snare_on
  "**MUSIC CONTROL** snare: #{is_snare_on ? "ON" : "OFF"}"
end

# down the tempo (castella)
hook 'vrchat-001', /pickup object: .Uguisu_Castella\.000/i do
  bpm -= 10
  "**MUSIC CONTROL** BPM: #{bpm}"
end

# up the tempo (castella)
hook 'vrchat-001', /pickup object: .Uguisu_Castella\.001/i do
  bpm += 10
  "**MUSIC CONTROL** BPM: #{bpm}"
end

# guitar (donut)
hook 'vrchat-001', /pickup object: .(Donuts_[^']*)/i do |e|
  inst_type = :guitar10

  case e.m[1]
  when /(ChocoFashion|ChocoFrench|ChocoMint|ChocoMochi|MusicalFashion)/
    inst_type = :guitar08
  when /(FrenchCruller|IchigoFrench)/
    inst_type = :guitar13
  end

  spawn "play", "-v", "#{inst_volume}", "-q", wav[inst_type], :err=>"/dev/null"
  "**MUSIC CONTROL** start : #{inst_type}"
end

# volume down (yunomi)
hook 'vrchat-001', /pickup object: .bds_yunomi\.000/i do
  inst_volume = (inst_volume - 0.2).round(1)
  inst_volume = 0.0 if inst_volume < 0
  "**MUSIC CONTROL** volume: #{inst_volume}"
end

# volume up (yunomi)
hook 'vrchat-001', /pickup object: .bds_yunomi\.001/i do
  inst_volume = (inst_volume + 0.2).round(1)
  "**MUSIC CONTROL** volume: #{inst_volume}"
end
