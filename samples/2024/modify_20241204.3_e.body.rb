# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-04
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241204.3
#

using OutvokeDSL

loputs "abc"

hooklo do
  e.upcase!
  "e: #{e} (#{e.class}) ---- e.body: #{e.body} (#{e.body.class})"
end
