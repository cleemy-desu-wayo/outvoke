# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-06
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241206
#
# usage:
#
#   $ { echo aaa ; sleep 3 ; echo bbb ; } | ./outvoke.rb samples/2024/modify_20241206_msgid.rb
#

using OutvokeDSL

hook ["stdin", "every-sec"] do
  "#{e} ---- #{e.msgid}"
end
