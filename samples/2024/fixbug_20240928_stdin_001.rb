# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-09-28
# --------
#
# this is a sample code to check for the presence of a bug that is fixed in
# Outvoke version 0.0.99.20240928.
#
# usage:
#
# $ seq 1 3 | ./outvoke.rb samples/2024/fixbug_20240928_stdin_001.rb
#

using OutvokeDSL

hook "stdin"
sleep 1
hook "stdin"
