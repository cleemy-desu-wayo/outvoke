# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-14
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241214.3
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241214.3_post_procs_cc_001.rb
#
# This sample would not make a difference if run on an older version of
# Outvoke. Because loputs returns nil.
#

using OutvokeDSL

hookcc "every-sec", /..:..:.[05]/ do
  sleep 2
  loputs e
end

hooklo
