# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-19
# --------
#
# this sample code is for checking the new features introduced in
# version 0.0.99.20241119.2
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241119.2_attachment.rb
#

using OutvokeDSL

str1 = "hello"
str2 = "how are you"

hooksec do
  next if $outvoke.elapsed > 2
  loputs str1, str2
  str1.upcase!    # object_id of str1 is not changed
  str2.upcase!    # object_id of str2 is not changed
  sleep 2
  nil
end

# it is intentional that the output result is "hello ---- HOW ARE YOU".
hooklo do |e|
  "#{e.body} ---- #{e.attachment}"
end
