# for Outvoke 0.1 (version 0.0.99.20240819 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-06
# --------
#
# a minimal sample of loputs and hookcc
#
# * Description-en:
#
#   After some time has passed since this sample was started, you
#   should see the line "hook start (puts)" output. The entire program
#   appears to stop for a while here, which is normal behavior.
#
#   By using hookcc instead of hook, the entire block can be executed
#   in another thread. hookcc was introduced in Outvoke version
#   0.0.99.20240817. "cc" stands for concurrent computing.
#
#   The string passed to loputs can be captured with the `hook "lo"`.
#   "lo" stands for loopback.
#
#   As an abbreviation for `hook "lo"`, it can be written `hooklo`.
#
#   See also samples/2024/loputs_hotaru_no_hikari.rb for a slightly
#   longer sample.
#
# * Description-ja:
#
#   このサンプルを起動してからしばらく経つと、「hook start (puts)」
#   という行が出力されるはずです。ここでしばらくプログラム全体が停止
#   したように見えますが、これは正常な挙動です。
#
#   hook ではなく hookcc を使用することにより、ブロック全体を別スレッド
#   で実行することが可能になります。Outvokeの version 0.0.99.20240817
#   から hookcc が登場しました。「cc」は concurrent computing です。
#
#   loputs に渡した文字列は、「hook "lo"」でキャプチャできます。
#   「lo」はループバックです。
#
#   「hook "lo"」の省略形として、「hooklo」と書くことができます。
#
#   少し長いサンプルとして、samples/2024/loputs_hotaru_no_hikari.rb も
#   ご覧ください。
#

using OutvokeDSL

hook 'every-sec'

hook 'every-sec', / ..:..:[024]0/ do
  puts   "hook start (puts)"
  loputs "hook start (loputs)"
  sleep 5
  puts   "hook end (puts)"
  loputs "hook end (loputs)"
end

hookcc 'every-sec', / ..:..:[135]0/ do
  puts   "hookcc start (puts)"
  loputs "hookcc start (loputs)"
  sleep 5
  puts   "hookcc end (puts)"
  loputs "hookcc end (loputs)"
end

hooklo
