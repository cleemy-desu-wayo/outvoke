# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-09-25
# --------
#
# this is a sample code to check for the presence of a bug that is fixed in
# Outvoke version 0.0.99.20240921.2.
#

using OutvokeDSL

hookcc "vrchat-001", /pickup object...([^']*)/i do |e|
  sleep 0.02
  loputs "object name: #{e.m[1]}"
end

hook "vrchat-001", /(dummy-dummy-dummy-63454865865341445453)/i do |e|
  sleep 0.05
  nil
end

hooklo
