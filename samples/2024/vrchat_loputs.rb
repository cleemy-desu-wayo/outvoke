# for Outvoke 0.1 (version 0.0.99.20240815 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-08-16
# --------
#
# a sample of loputs
#
# strings passed as arguments to loputs can be captured by the data source "lo".
# see also samples/2024/loputs_api_access.rb
#

using OutvokeDSL

$outvoke.wait = 0.1

hook "vrchat-001", /pickup object...([^']*)/i do |e|
  object_name = e.m[1]
  loputs "#{object_name} : hook start"

  Thread.start do
    loputs "#{object_name} : ---- thread start (#{Time.now}) ----"

    sleep 2
    loputs "#{object_name} : 2 seconds elapsed after pickup"
    sleep 3
    loputs "#{object_name} : 5 seconds elapsed after pickup"
    sleep 5
    loputs "#{object_name} : 10 seconds elapsed after pickup"

    loputs "#{object_name} : ---- thread end (#{Time.now}) ----"
  end

  loputs "#{object_name} : hook end"
end

hook "lo", /./ do |e|
  "loopback event - #{e.body}"
end
