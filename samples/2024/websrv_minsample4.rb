# for Outvoke 0.1 (version 0.0.99.20241127 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-27
# --------
#
# a sample of data source "web-001"
#
# in this example, there may not be much point in using Outvoke.
#

using OutvokeDSL

#
# try visiting http://localhost:8081 in your browser
#
$outvoke["web-001"].then do
  _1.port           = 8081
  _1.document_root  = "./"
  _1.mount_proc_dir = nil
  _1.enabled        = true
end
