# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-12
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241112
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241112_enabled.rb
#
#   (VRChat required)
#

using OutvokeDSL

hook "every-sec", / ..:..:.0/ do
  hook "vrchat-001", /pickup/i
end
