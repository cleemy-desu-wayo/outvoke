# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-12-16
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241216
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241216_loputs.rb
#

using OutvokeDSL

hooksec /..:..:.[02468]/ do
  loputs "a"
end

hooksec /..:..:.[13579]/ do
  loputs
end

hooklo do
 "hooklo: #{e}"
end
