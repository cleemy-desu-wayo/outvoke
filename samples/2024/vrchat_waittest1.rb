# for Outvoke 0.1 (version 0.0.99.20240521 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-05-23
# --------
#
# This sample works similar to samples/2024/vrchat_waittest2.rb.
#
# * Setup:
#
#   1. Save as "maoudamashii-se-system47.wav" from https://maou.audio/se_system47/
#
#   2. Place this .wav file, outvoke.rb, and this sample (vrchat_waittest1.rb) in the
#      current directory, and grant execute permission to outvoke.rb.
#      Then execute as follows.
#
#      $ ./outvoke.rb vrchat_waittest1.rb
#
#   3. Start VRChat and go to "猫和室 - modern".
#      (World ID: wrld_3a201318-33c2-4c4c-8310-163552f80d63)
#
# * Description-en:
#
#   A sound is made only when you pick up a dango. When you pick up a yunomi (japanese teacup)
#   or kyusu (japanese teapot), the interval of the main loop changes.
#
#   After you pick up yunomi, the interval will be set to "5", and from then on, the sound
#   will sound at most 5 seconds after you pick up a dango.
#   After you pick up kyusu, the interval will be set to "0.1", and from then on, the sound
#   will sound immediately (within 0.1 seconds) after you pick up a dango.
#
#   VRChat world "猫和室 - modern" was chosen only as an example of a world with plenty of
#   objects to pick up. This sample may not work due to changes in the specifications of
#   VRChat or updates to the world. Even if this sample doesn't work as expected, please
#   do not contact the world creator.
#   (BTW, "猫" (neko) means cats, and "和室" (washitsu) means japanese room)
#
# * Description-ja:
#
#   だんごをpickupした時のみ音が鳴ります。
#   湯呑みや急須をpickupすると、メインループのウェイトの秒数が変わります。
#
#   湯呑みをpickupするとウェイトは「5」に設定され、それ以降はだんごをpickupしてから最大5秒後
#   に音が鳴るようになります。
#   急須をpickupするとウェイトは「0.1」に設定され、それ以降はだんごをpickupしてからすぐ
#   （0.1秒以内）に音が鳴るようになります。
#
#   VRChatのワールド「猫和室 - modern」はあくまでもpickupするものが豊富にあるワールドの
#   例として選んだだけです。VRChatの仕様変更やワールドのアップデートにより、このサンプルは
#   動かなくなる可能性があります。もしこのサンプルが期待通りに動かなかったとしても、
#   ワールド制作者さんに問い合わせをしないようお願いします。
#

using OutvokeDSL

hook 'vrchat-001', /pickup object: .(bds_yunomi[^']*)/i do |e|
  $outvoke.wait = 5
  puts "new interval: #{$outvoke.wait}"
  e.m[1]
end

hook 'vrchat-001', /pickup object: .(bds_kyusu[^']*)/i do |e|
  $outvoke.wait = 0.1
  puts "new interval: #{$outvoke.wait}"
  e.m[1]
end

hook 'vrchat-001', /pickup object: .(uguisu_3dango[^']*)/i do |e|
  spawn "play", "-v", "0.2", "-q", "maoudamashii-se-system47.wav", :err=>"/dev/null"
  e.m[1]
end
