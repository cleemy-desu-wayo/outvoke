# for Outvoke 0.1 (version 0.0.99.20240815 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-08-16
# --------
#
# a sample of loputs
#
# strings passed as arguments to loputs can be captured by the data source "lo".
# see also samples/2024/vrchat_loputs.rb
#

using OutvokeDSL

require 'net/http'
require 'uri'
require 'json'
require 'time'

def https_get(uri_str)
  uri = URI.parse(uri_str)
  https = Net::HTTP.new(uri.host, uri.port)

  https.use_ssl     = true
  https.verify_mode = OpenSSL::SSL::VERIFY_PEER

  ###
  ### if you got a SSL/TLS-related error, try ca_file
  ### you can get .pem file from https://curl.se/docs/caextract.html
  ###
  # https.ca_file = './cacert.pem'

  # HTTPS access
  https_error = ""
  begin
    response = https.request(Net::HTTP::Get.new(uri.request_uri))
  rescue => e
    https_error = "fatal error - #{e.message}"
  end

  if https_error.empty?
    begin
      response.value
    rescue => e
      https_error = e.message
    end
  end

  [response, https_error]
end

releases_list = [
  {
    software_name: 'Proton GE',
    url:           'https://api.github.com/repos/GloriousEggroll/proton-ge-custom/releases',
    schedule:      / [012][0-9]:[03]0:00/    # once every 30 minutes
  },
  {
    software_name: 'yt-dlp',
    url:           'https://api.github.com/repos/yt-dlp/yt-dlp/releases',
    schedule:      / [012][02468]:00:00/    # once every 2 hours
  }
]

releases_list.each do |releases_info|
  hook "every-sec", releases_info[:schedule] do
    Thread.start do
      sleep rand(120..480)    # avoid concentrated access

      response, https_error = https_get(releases_info[:url])
      fetched_json_text = response&.body

      fetched_data = nil
      json_error = ""
      if fetched_json_text && https_error == ""
        begin
          fetched_data = JSON.parse(fetched_json_text)
        rescue => e
          json_error = "#{e.class}"
        end
      end

      if fetched_data.class == Hash && fetched_data['message'] =~ /rate limit/i
        loputs "#{releases_info[:software_name]} -- ???? -- ERROR: rate limit (#{https_error})"
      elsif fetched_data.class == Array && fetched_data.first['tag_name']
        fetched_data.first.then do
          published_date = Time.parse(_1['published_at']).localtime
          loputs "#{releases_info[:software_name]} -- #{_1['tag_name']} -- (published: #{published_date})"
        end
      else
        loputs "#{releases_info[:software_name]} -- ???? -- ERROR: #{https_error}#{json_error}"
      end
    end

    nil
  end
end

hook "lo", /./
