# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-19
# --------
#
# this sample code is for checking the changes in version 0.0.99.20241119.1
#
# usage:
#
#   $ ./outvoke.rb samples/2024/modify_20241119.1_loputs.rb
#

using OutvokeDSL

str = "hello"

hooksec do
  next if $outvoke.elapsed > 2
  loputs str
  str.upcase!    # object_id of str is not changed
  sleep 2
  nil
end

hooklo
