# for Outvoke 0.1 (version 0.0.99.20240819 or later)
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-04
# --------
#
# a sample of playing "Hotaru no Hikari"
#
# * Description-en:
#
#   When 22:48 is reached, just set a flag and then it will start
#   life-and-death monitoring every second and execute mpv.
#   If you exit mpv by mistake, it will immediately start playing again.
#
#   In this simple example, the `loputs "music start"` part is the same
#   as simply `"music start"`. However, if you have in mind the
#   possibility of future extensions, it makes sense to aggregate them
#   in the data source lo.
#
#   For example, consider the possibility that the `hooklo` part might
#   be changed to the following three lines:
#
#     hooklo do |e|
#       "[music-control] #{e.body}"
#     end
#
#   As for yt-dlp, the latest version is available from
#   https://github.com/yt-dlp/yt-dlp/releases/.
#
#   If yt-dlp_linux is in the current directory, try changing the 'false'
#   part to '. /yt-dlp_linux'. The video title is obtained by yt-dlp.
#
# * Description-ja:
#
#   22:48 になると、フラグを立てることだけをして、あとは1秒に1回の
#   死活監視によって mpv を起動します。もし間違って mpv を終了して
#   しまったとしても、すぐにまた起動します。
#
#   このようなシンプルな例では、「loputs "music start"」の箇所は単に
#   「"music start"」とするのと同じです。しかしながら、もし将来的な拡張
#   の可能性を念頭に置いているなら、データソース lo に集約させておく
#   ことには意味があります。
#
#   例えば、「hooklo」の箇所が以下のような3行に変更されるかもしれない
#   という可能性を考えてみてください。
#
#     hooklo do |e|
#       "[music-control] #{e.body}"
#     end
#
#   yt-dlp については、https://github.com/yt-dlp/yt-dlp/releases/ から
#   最新版を入手可能です。
#
#   カレントディレクトリに yt-dlp_linux がある場合、'false' の箇所を
#   './yt-dlp_linux' に変更してみてください。yt-dlp によって動画タイトル
#   を取得します。
#

using OutvokeDSL

# if already initialized in outvoke.conf.rb, these values are ignored.
$hotaru_no_hikari_video_id   ||= 'OgYWssWn7uQ'
$hotaru_no_hikari_start_time ||= / 22:48:00/
$hotaru_no_hikari_stop_time  ||= / 23:05:00/
$hotaru_no_hikari_ytdlp_path ||= 'false'

mpv_pid     = -1
mpv_thread  = nil
is_music_on = false

hook 'every-sec', $hotaru_no_hikari_start_time do
  is_music_on = true
  loputs "music start"
end

hookcc 'every-sec' do
  next unless is_music_on
  next if     mpv_thread&.alive?

  loputs "detected that mpv is not running..."

  mpv_pid = spawn "mpv",
                  "https://www.youtube.com/watch?v=#{$hotaru_no_hikari_video_id}",
                  "--really-quiet",
                  "--loop",
                  :err=>"/dev/null"
  mpv_thread = Process.detach(mpv_pid)

  video_title = IO.popen([$hotaru_no_hikari_ytdlp_path,
                          "-q",
                          "--get-title",
                          "https://www.youtube.com/watch?v=#{$hotaru_no_hikari_video_id}",
                          :err=>"/dev/null"]
                        ).each_line.first 

  loputs "mpv executed (pid: #{mpv_pid} -- video_id: #{$hotaru_no_hikari_video_id} -- video_title: #{video_title})"
end

hook 'every-sec', $hotaru_no_hikari_stop_time do
  is_music_on = false
  Process.kill('TERM', mpv_pid)
  loputs "music stop"
end

hooklo
