# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-09-28
# --------
#
# this is a sample code to check for the presence of a bug that is fixed in
# Outvoke version 0.0.99.20240928.
#
# usage:
#
# $ { i=0 ; while true ; do echo "$i" ; sleep 1 ; i=$(expr "$i" + 1) ; done ; } | ./outvoke.rb samples/2024/fixbug_20240928_stdin_002.rb
#

using OutvokeDSL

hook "every-sec", / ..:..:.[0]/ do |e|
  $outvoke.sources["stdin"].enabled = false
  "set false ---- #{e.body}"
end

hook "every-sec", / ..:..:.[3]/ do |e|
  $outvoke.sources["stdin"].enabled = true
  "set true  ---- #{e.body}"
end

hook "stdin"

$outvoke.sources["stdin"].enabled = false
