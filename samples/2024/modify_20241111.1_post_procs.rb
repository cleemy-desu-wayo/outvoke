# for Outvoke 0.1
# written by cleemy desu wayo / Licensed under CC0 1.0
# initial release of this sample: 2024-11-11
# --------
#
# this sample code is for checking the new features introduced in
# version 0.0.99.20241111.1
#
# usage:
#
#   $ seq 1 7 | ./outvoke.rb samples/2024/modify_20241111.1_post_procs.rb
#
#   (try also with -q option)
#
#   $ seq 1 7 | ./outvoke.rb -q samples/2024/modify_20241111.1_post_procs.rb
#

using OutvokeDSL

proc_buf = $outvoke.sources["stdin"].post_procs[0]

hookstd /2/ do
  $outvoke.sources["stdin"].pre_procs << ->(e) {
    puts "pre-process A ---- #{e.body}"
  }

  $outvoke.sources["stdin"].post_procs[0] = ->(e, x) {
    return unless x
    puts "post-process A ---- #{x}"
  }

  nil
end

hookstd /4/ do |e|
  $outvoke.sources["stdin"].pre_procs << ->(e) {
    puts "pre-process B ---- #{e.body}"
    e.body = "#{e.body}-#{e.body}"
  }

  puts "e.body: #{e.body}"

  $outvoke.sources["stdin"].post_procs << ->(e, x) {
    return unless x
    puts "post-process B ---- #{x}"
  }

  nil
end

hookstd /6/ do
  $outvoke.quiet_mode = !($outvoke.quiet_mode?)
  $outvoke.sources["stdin"].post_procs[0] = proc_buf
  nil
end

hookstd do
  "hookstd #{_1.body}"
end
